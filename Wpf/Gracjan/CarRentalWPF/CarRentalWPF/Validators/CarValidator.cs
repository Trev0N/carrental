﻿using System.Globalization;
using System.Windows.Controls;

namespace CarRentalWPF.Validators
{
    /// <summary>
    /// CarModel - Mark Property
    /// </summary>
    public class CarMarkValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Mark Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Model Property
    /// </summary>
    public class CarModelValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Model Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Type Property
    /// </summary>
    public class CarTypeValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Type Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Engine Property
    /// </summary>
    public class CarEngineValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Engine Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Power Property
    /// </summary>
    public class CarPowerValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Power Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Mileage Property
    /// </summary>
    public class CarMileageValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Mileage Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Status Property
    /// </summary>
    public class CarStatusValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Status Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - Plate Property
    /// </summary>
    public class CarPlateValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make Plate Validator

            return ValidationResult.ValidResult;
        }
    }

    /// <summary>
    /// CarModel - VIN Property
    /// </summary>
    public class CarVINValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // TODO: Make VIN Validator

            return ValidationResult.ValidResult;
        }
    }
}
