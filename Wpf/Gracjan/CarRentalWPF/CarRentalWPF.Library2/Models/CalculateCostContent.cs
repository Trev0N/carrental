﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPF.Library.Models
{
    public class CalculateCostContent
    {
        public string carVin { get; set; }

        public string rentStartDate { get; set; }

        public string rentEndDate { get; set; }
    }
}
