﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPF.Library.Models
{
    public class NewAgencyContent
    {
        public string city;

        public string country;

        public string flatNo;

        public string houseNo;

        public int maxCarQuantity;

        public string postalCode;

        public string street;
    }
}