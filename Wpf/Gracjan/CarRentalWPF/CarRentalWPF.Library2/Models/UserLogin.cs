﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPF.Library.Models
{
    public class UserLogin
    {
        public string username { get; set; }

        public string password { get; set; }
    }
}
